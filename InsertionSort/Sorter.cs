using System;

#pragma warning disable SA1611

namespace InsertionSort
{
    public static class Sorter
    {
        public static void InsertionSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            for (int i = 0; i < array.Length; i++)
            {
                int container = 0;
                int k = i;

                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] < array[j])
                    {
                        break;
                    }
                    else if (array[i] > array[j] && i != 0)
                    {
                        container = array[j];

                        while (container < array[k - 1] && k > 1)
                        {
                            k--;
                        }

                        if (container < array[k - 1])
                        {
                            for (int z = i; z >= 0; z--)
                            {
                                array[z + 1] = array[z];
                            }

                            array[k - 1] = container;
                            break;
                        }
                        else
                        {
                            array[j] = array[k];
                            array[k] = container;
                            break;
                        }
                    }
                    else if (array[i] > array[j] && i == 0)
                    {
                        container = array[j];
                        array[j] = array[i];
                        array[i] = container;
                        break;
                    }
                }
            }
        }

        public static void RecursiveInsertionSort(this int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            RecursionHelper(array, array.Length);
        }

        public static void RecursionHelper(int[] array, int n)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array), "Array cannot be null.");
            }

            if (n <= 1)
            {
                return;
            }

            RecursionHelper(array, n - 1);

            int container = array[n - 1];
            int j = n - 2;

            while (j >= 0 && array[j] > container)
            {
                array[j + 1] = array[j];
                j--;
            }

            array[j + 1] = container;
        }
    }
}
